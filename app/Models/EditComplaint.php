<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class EditComplaint extends Model
{
    protected $primaryKey = 'e_id';
    protected $table = 'edit_complaint_data';
    public $timestamps = false;
}