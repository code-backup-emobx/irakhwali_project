<?php if($errors->any()): ?>
<div class="clerifix">&nbsp;</div>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<?php if(\Illuminate\Support\Facades\Session::has('success')): ?>
<div class="clerifix">&nbsp;</div>
    <div class="alert alert-success">
        <button class="close" data-close="alert"></button>
        <?php echo e(Session::get('success')); ?>

    </div>
<?php endif; ?>
<?php if(\Illuminate\Support\Facades\Session::has('fail')): ?>
<div class="clerifix">&nbsp;</div>
    <div class="alert alert-danger">
        <button class="close" data-close="alert"></button>
        <ul>
            <li><?php echo e(Session::get('fail')); ?></li>
           
        </ul>
    </div>
<?php endif; ?>
<?php if(\Illuminate\Support\Facades\Session::has('warning')): ?>
<div class="clerifix">&nbsp;</div>
    <div class="alert alert-warning">
        <button class="close" data-close="alert"></button>
        <ul>
            
                <li><?php echo e(Session::get('warning')); ?></li>
           
        </ul>
    </div>
<?php endif; ?><?php /**PATH E:\emobx_office\htdocs\irakhwali\resources\views/layouts/notification.blade.php ENDPATH**/ ?>