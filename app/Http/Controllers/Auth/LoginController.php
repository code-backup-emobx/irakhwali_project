<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request){
        
//        $obj = new Utilities();
//        $obj->pre($request->all());
          if (\Auth::attempt(['name' => $request->name, 'password' => $request->password, 'user_type'=> 'super_admin'])) {
            // Authentication passed...
            return redirect()->intended('/')->with('success','Welcome Back');
        }else{
            return redirect()->back()->with('fail','!Enter the valid email and password');
        }
    }
}
