
<?php $__env->startSection('content'); ?>
        
<div style="padding-top: 2%"></div>
    <div class="page-content-wrapper">
        <div class="page-content">
            
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">

                            <?php if(Session::has('success')): ?>

                                <div class="alert alert-success">

                                    <?php echo e(Session::get('success')); ?>


                                    <?php

                                    Session::forget('success');

                                    ?>

                                </div>

                            <?php endif; ?>
                            
                            <div class="caption">
                                <!-- <i class="icon-bubble font-dark"></i> -->
                                <span class="caption-subject font-dark bold">Officer Area list</span>
                            </div>

                            <div class="col-md-10">
                                <div class="col-lg-3 action" style="margin-left:5px">
                                    <form class="form-horizontal">
                                        <select name="division_id" id="division_id"  class="form-control">
                                            <option value="">Select Division</option>
                                            <?php $__currentLoopData = $division_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $listing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <option value="<?php echo e($listing->id); ?>" <?php if($division_id == $listing->id): ?> selected <?php endif; ?> ><?php echo e($listing->division_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div> 
                                        <input type="button" class="btn btn-success" value="Submit" style="float:left; margin-right:20% "  onclick="return mysubmit();">
                                    </form>

                                </div>
                            </div>
                         

                            <?php if(Request::segment(2)): ?> 

                            <div class="portlet-body">
                                <?php if(!empty($area_detail)): ?>
                                    <div class="table-scrollable">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr> 
                                                    <td colspan="2">Division Name</td> 
                                                    <td colspan="2"><?php echo e($area_detail->division_name); ?></td> 
                                               </tr> 
                                               <tr> 
                                                    <td>Dfo Name</td> 
                                                    <td><?php echo e($area_detail->dfo_name); ?></td> 
                                                    <td>Phone No</td> 
                                                    <td><?php echo e($area_detail->dfo_contact); ?></td>
                                               </tr> 
                                           </tbody>
                                        </table>
                                    </div>
                                <?php endif; ?>
                                <!-- 2 -->
                                <?php //echo "<pre>"; print_r($area_detail); die(); ?>
                                <?php //echo "<pre>"; print_r($area_detail[0]['forestblocks']); die(); ?>

                                <?php $__currentLoopData = $area_detail['forestranges']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $forestrange): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="row" style="padding-left:2.5%;padding-right:1%;">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr> 
                                                        <td colspan="2">Range Name</td> 
                                                        <td colspan="2"><?php echo e($forestrange->range_name); ?></td>
                                                    </tr> 
                                                    <tr> 
                                                        <td>Ranger Officer</td> 
                                                        <td><?php echo e($forestrange->ro_name); ?></td> 
                                                        <td>Phone No</td> 
                                                        <td><?php echo e($forestrange->ro_contact); ?></td>
                                                    </tr> 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- 2 -->

                                    <?php $__currentLoopData = $area_detail['forestblocks']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $forestblock): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                        <?php //echo "<pre>"; print_r($area_detail); die(); ?>
                                       
                                        <?php if($forestblock->range_id ==  $forestrange->id): ?>
                                            <div class="row" style="padding-left:3.5%;padding-right:1%;">
                                                <div class="table-scrollable">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr> 
                                                                <td colspan="2">Block Name</td> 
                                                                <td colspan="2"><?php echo e($forestblock->block_name); ?></td>
                                                            </tr> 
                                                            <tr> 
                                                                <td>Block Officer</td> 
                                                                <td><?php echo e($forestblock->bo_name); ?></td> 
                                                                <td>Phone No</td> 
                                                                <td><?php echo e($forestblock->bo_contact); ?></td>
                                                            </tr> 
                                                        </tbody>
                                                    </table>
                                                </div> 
                                           </div>
                                       
                                  
                                            <!-- /2 -->
                                            <?php //echo "<pre>"; print_r($area_detail[0]['forestbeats']); die(); ?>
                                            <!-- 3 -->
                                            <?php $__currentLoopData = $area_detail['forestbeats']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $forestbeat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               
                                                
                                                <?php if($forestbeat->block_id ==  $forestblock->id): ?>
                                                    
                                                    <div class="row" style="padding-left:4.5%;padding-right:1%;">
                                                        <div class="table-scrollable">
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                    <tr> 
                                                                        <td colspan="2">Beat Name</td> 
                                                                        <td colspan="2"><?php echo e($forestbeat->beat_name); ?></td>
                                                                    </tr> 
                                                                    <tr> 
                                                                        <td>Guard Name</td> 
                                                                        <td><?php echo e($forestbeat->g_name); ?></td> 
                                                                        <td>Phone No</td> 
                                                                        <td><?php echo e($forestbeat->g_contact); ?></td>
                                                                    </tr> 
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <!-- /3 -->
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                            <?php endif; ?>
                                

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
    <!-- END CONTENT -->
    </div>
<script>
var crt_url = "<?php echo url('/'); ?>";
function mysubmit(){
  //you can return false; here to prevent the form being submitted
  //useful if you want to validate the form
  var id = $( "#division_id" ).val();
  
  uri =  crt_url + '/area_officer_data/'+ document.getElementById('division_id').value;
  window.location.href= uri;
}
</script>
       
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/divisiondata/area_officer_data.blade.php ENDPATH**/ ?>