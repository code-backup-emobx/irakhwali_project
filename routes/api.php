<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user_registration','ApiController@user_registration');
Route::post('post_to_url','ApiController@post_to_url');
Route::post('sendSingleSMS','ApiController@sendSingleSMS');
Route::post('login','ApiController@login');
Route::post('logout','ApiController@logout');
Route::post('user_complaint','ApiController@user_complaint');
Route::post('fetch_previous_complaint','ApiController@fetch_previous_complaint');
Route::post('edit_complaint','ApiController@edit_complaint');
Route::get('nature_of_complaint', 'ApiController@nature_of_complaint');
Route::post('update_register_complaint','ApiController@update_register_complaint');

// pun_forest_app api

Route::get('get_division','ApiController@get_division');
Route::post('get_range', 'ApiController@get_range');
Route::post('get_block', 'ApiController@get_block');
// Route::post('get_district', 'ApiController@get_district');

Route::post('get_beat','ApiController@get_beat');
Route::post('login_punforest_app','ApiController@login_punforest_app');
Route::post('guard_action' , 'ApiController@guard_action');
Route::post('action','ApiController@action');



