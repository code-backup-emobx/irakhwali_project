<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestBlock extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_block';
   	public $timestamps = false;

   	public function forestrange(){

    	return $this->belongsTo('App\Models\ForestRange');
    }

    public function forestbeats(){

    	return $this->hasMany('App\Models\ForestBeat');
    }
}