<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AppUsers extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'app_users';
}