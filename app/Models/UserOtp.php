<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserOtp extends Model
{
    protected $primaryKey = 'uo_id';
    protected $table = 'user_otp';
    public $timestamps = false;
}