@extends('layouts.master')
@section('content')
        
 <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                         <div class="caption">
                                            <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i>
                                            <span class="caption-subject">Division Data</span>
                                        </div>
                                        
                                    </div>
                                  </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                            <th>Division List</th>
                                            <th>Add Detail</th>
                                        <tr>
                                          
                                            
                                            @foreach($division_data as $listing)
                                            <tr>
                                                <td>&nbsp;&nbsp;{{$listing->id}}.&nbsp;&nbsp;{{$listing->division_name}} </td>
                                                <td><a href="{{ url('/add_dfodetail/'.$listing->id) }}" class="btn btn-success" >Add detail</a></td>
                                                
                                            </tr>
                                            @endforeach
                                          
                                        </tbody>
                                    </table>
                                        </div>
                                    
                                    </div>
                                </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
       
@endsection

