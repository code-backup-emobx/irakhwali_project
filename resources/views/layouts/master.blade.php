@include('layouts.top_bar')

   
            <!-- BEGIN sidebar -->
            @include('layouts.sidebar')
            <!-- END sidebar -->
            <div class="content">
            @include('layouts.notification')
                   
           @yield('content')
            <!-- BEGIN FOOTER -->
            @include('layouts.footer')
            <!-- END FOOTER -->