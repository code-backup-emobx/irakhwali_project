<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestRange extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_range';
   	public $timestamps = false;

	public function forestdivision(){

    	return $this->belongsTo('App\Models\ForestDivision');
    }

    public function forestblocks(){

    	return $this->hasMany('App\Models\ForestBlock','range_id');
    }
}