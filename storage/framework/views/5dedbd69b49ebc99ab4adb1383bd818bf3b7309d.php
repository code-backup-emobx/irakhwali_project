 <div style="padding-top: 2%">
 <div class="page-sidebar-wrapper">
 <!-- BEGIN SIDEBAR -->
                   
            <div class="page-sidebar navbar-collapse collapse">
                
                <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                    <li class="sidebar-toggler-wrapper hide">
                        <div class="sidebar-toggler">
                        </div>
                    </li>
                    <div class="clearfix" style="padding-top: 10%;"></div>
                    <li class="nav-item start active open">
                        <a href="/" class="nav-link nav-toggle">
                            <i class="icon-home"></i>
                            <span class="title">Dashboard</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>
                    <li class="heading">
                        <h3 class="uppercase">Features</h3>
                    </li>
                   
                    <li class="nav-item  ">
                        <a href="/complaint_list" class="nav-link nav-toggle">
                            <i class="icon-diamond"></i>
                            <span class="title">Complaints</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="/user_list" class="nav-link nav-toggle">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="title">Users</span>
                        </a>
                    </li>

                     <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-diamond"></i>
                            <span class="title">Forest Officer</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            
                            <li class="nav-item">
                                <a href="/area_officer_data" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Area Officer's Data</span>
                                </a>
                            </li>

                             <li class="nav-item">
                                <a href="/division_data" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Division Data</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    
                    
                    <!-- <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-puzzle"></i>
                            <span class="title">Components</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="components_knob_dials.html" class="nav-link ">
                                    <span class="title">Knob Circle Dials</span>
                                </a>
                            </li>
                        </ul>
                    </li> -->

                     

                    <!-- <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-bulb"></i>
                            <span class="title">Elements</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="elements_cards.html" class="nav-link ">
                                    <span class="title">User Cards</span>
                                </a>
                            </li>
                        </ul>
                    </li> -->
                   
                   <!--  <li class="nav-item  ">
                        <a href="?p=" class="nav-link nav-toggle">
                            <i class="icon-wallet"></i>
                            <span class="title">Portlets</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                                <a href="portlet_draggable.html" class="nav-link ">
                                    <span class="title">Draggable Portlets</span>
                                </a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-pointer"></i>
                            <span class="title">Maps</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="maps_google.html" class="nav-link ">
                                    <span class="title">Google Maps</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="maps_vector.html" class="nav-link ">
                                    <span class="title">Vector Maps</span>
                                </a>
                            </li>
                        </ul>
                    </li> -->
                </ul>
                <!-- END SIDEBAR MENU -->
                <!-- END SIDEBAR MENU -->
            </div>
</div>
       <?php /**PATH E:\emobx_office\htdocs\irakhwali\resources\views/layouts/sidebar.blade.php ENDPATH**/ ?>