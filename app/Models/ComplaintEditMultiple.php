<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ComplaintEditMultiple extends Model
{
    protected $primaryKey = 'e_id';
    protected $table = 'edit_complaint_multiple';
    public $timestamps = true;
}