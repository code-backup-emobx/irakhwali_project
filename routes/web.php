<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', function(){
// 	return view('auth/login');
// });

// Auth::routes();


Route::post('login','LoginController@login')->name('login');
Route::get('login', 'LoginController@forest_division');


// Route::get('/home', 'HomeController@index');
//  Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'DashboardController@showCountdataDashboard');
Route::get('complaint_list/{search?}','ComplaintController@complaintList');
Route::get('edit_complaint/{id}','ComplaintController@editComplaint');
Route::post('edit_complaint/{id}','ComplaintController@updateComplaint');
Route::get('user_list/{search?}','UserController@userlist');
Route::get('division_data','DivisionController@division_data');
// listing of officers using filter
Route::get('area_officer_data/{id?}','DivisionController@areaofficer_data');

Route::get('add_dfodetail/{id}','DivisionController@edit_dfodetail');
Route::post('add_dfodetail/{id}','DivisionController@add_dfo_detail');
Route::get('range/{id}','DivisionController@range_detail');
Route::get('add_rodetail/{id}','DivisionController@rodetail');
Route::post('add_rodetail/{id}','DivisionController@add_ro_detail');
Route::get('block/{id}','DivisionController@block_list');
Route::get('add_bodetail/{id}','DivisionController@bodetail');
Route::post('add_bodetail/{id}','DivisionController@add_bo_detail');
Route::get('beat/{id}','DivisionController@beat_list');
Route::get('add_guard_detail/{id}','DivisionController@guard_detail');
Route::post('add_guard_detail/{id}','DivisionController@add_guard_detail');









