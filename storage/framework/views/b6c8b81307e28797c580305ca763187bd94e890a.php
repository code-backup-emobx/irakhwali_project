  
  
  <?php $__env->startSection('content'); ?>

    <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i>
                                <span class="caption-subject">Edit Complaint</span>
                            </div>
                            <div class="actions">
                              
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div>

                            <?php if(Session::has('success')): ?>

                            <div class="alert alert-success">

                                <?php echo e(Session::get('success')); ?>


                                <?php

                                Session::forget('success');

                                ?>

                            </div>

                            <?php endif; ?>
                            <form action="<?php echo e(url('/edit_complaint/'.$view_particular_complaint->c_id)); ?>" class="form-horizontal" method="post">
                                  <?php echo e(csrf_field()); ?>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Complaint Media: </label>
                                        <div class="col-lg-7">
                                            <!--  -->
                                            <table class="table table-bordered">
                                                <thead>
                                                   <tr>
                                                        <th style="width:30%">Date</td>
                                                        <th style="width:30%">Media Type</td>
                                                        <th>Media</td>
                                                    </tr> 
                                                </thead>
                                                <tbody>
                                                    
                                                    <!-- <?php $__currentLoopData = $view_particular_complaint['complaint_media']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <tr>
                                                    <td><?php echo e($list->uploaded_on); ?></td>
                                                     <td><?php echo e($list->media_type); ?></td>
                                                    <?php if($list->media_type=='image'): ?>
                                                    <td> <img src="<?php echo e($list->media_url); ?>" width="200px" height="150px" style="padding:2px; float:left"></td>
                                                       <?php elseif($list->media_type=='video'): ?>
                                                    <td> 
                                                     <video  width="200" height="150"  controls >
                                                        <source src="<?php echo e($list->media_url); ?>" type="video/mp4">
                                                    </video> 
                                                    </td>
                                                    <?php else: ?> 
                                                    <td> 
                                                     <audio controls>
                                                        <source src="<?php echo e($list->media_url); ?>" type="video/mp4">
                                                    </audio> 
                                                    </td> 
                                                        
                                                    </tr>
                                                    <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  -->

                                                     
                                                   <?php $__currentLoopData = $complaint_media; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <tr>
                                                    <td><?php echo e($list->uploaded_on); ?></td>

                                                     <td><?php echo e($list->media_type); ?></td>

                                                    <?php if($list->media_type=='image'): ?>
                                                    <td><img src="<?php echo e($list->media_url); ?>" width="200px" height="150px" style="padding:2px; float:left"></td>

                                                    <?php elseif($list->media_type=='video'): ?>
                                                    <td>
                                                     <video width="200" height="150" controls>
                                                        <source src="<?php echo e($list->media_url); ?>"  type="video/mp4">
                                                    </video> 
                                                    </td>

                                                    <?php else: ?> 
                                                    <td> 
                                                     <audio controls>
                                                        <source src="<?php echo e($list->media_url); ?>" type="video/mp4">
                                                    </audio> 
                                                    </td> 
                                                        
                                                    </tr>
                                                    <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                    <!--  <tr>
                                                       
                                                        <td >MT</td>
                                                        <td>M</td>
                                                    </tr>

                                                     <tr>
                                                       
                                                        <td >MT</td>
                                                        <td>M</td>
                                                    </tr> -->
                                                  
                                                </tbody>
                                              </table>
                                           <!--  <?php $__currentLoopData = $view_particular_complaint['complaint_media']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <?php if($list->media_type=='image'): ?>
                                                <img src="<?php echo e($list->media_url); ?>" width="150px" height="150px" style="padding:5px; float:left">
                                                <?php elseif($list->media_type=='audio'): ?>
                                                <audio controls>
                                                  <source src="<?php echo e($list->media_url); ?>" type="audio/mp3" style="float:left;">
                                                </audio>
                                                  <?php else: ?>
                                                <video controls autoplay>
                                                  <source src="<?php echo e($list->media_url); ?>" type="video/mp4">
                                                </video>

                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> -->
                                        </div>
                                       
                                     </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Complaint Description: </label>
                                        <div class="col-lg-5">
                                           <table class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    <th>Date</th>
                                                    <th>Description</th>
                                                     <!-- <span class="text-danger"><?php echo e($errors->first('c_description')."only alpha value"); ?></span> -->
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                 
                                                  
                                                    <tr>
                                                        <td><?php echo e($view_particular_complaint->created_at); ?></td>
                                                        <td><?php echo e($view_particular_complaint->c_description); ?></td>
                                                    </tr> 
                                                
                                                
                                                   <?php $__currentLoopData = $view_multiple_complaint['complaint_description']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $listing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($listing->created_at); ?></td>
                                                        <td><?php echo e($listing->c_description); ?></td>
                                                    </tr>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                 
                                                   
                                                </tbody>
                                              </table>
                                             
                                           
                                        </div>
                                    </div>
                                   
                                     <div class="form-group row">
                                        <label class="col-lg-3 col-form-label"> Nature Of Complaint: </label>
                                        <div class="col-lg-5">
                                            <select class="form-control" name="nature_of_complaint" disabled="nature_of_complaint">
                                               
                                                <option value="1" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Tree cutting on Forest Land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਰੁੱਖਾਂ ਦੀ ਕਟਾਈ' ? 'selected="selected"' : ''); ?>> Tree cutting on Forest Land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਰੁੱਖਾਂ ਦੀ ਕਟਾਈ </option>

                                                <option value="2" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Encroachment/Construction on Forest land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਕਬਜ਼ਾ/ਉਸਾਰੀ' ? 'selected="selected"' : ''); ?>> Encroachment/Construction on Forest land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਕਬਜ਼ਾ/ਉਸਾਰੀ </option>

                                                <option value="3" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Mining on Forest Land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਮਾਇਨਿੰਗ' ? 'selected="selected"' : ''); ?>> Mining on Forest Land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਮਾਇਨਿੰਗ </option>

                                                <option value="4" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Levelling of PLPA Lands / ਪੀ.ਐਲ.ਪੀ.ਏ ਦੀਆਂ ਜ਼ਮੀਨਾਂ ਨੂੰ ਪੱਧਰ ਕਰਨਾ' ? 'selected="selected"' : ''); ?>> Levelling of PLPA Lands / ਪੀ.ਐਲ.ਪੀ.ਏ ਦੀਆਂ ਜ਼ਮੀਨਾਂ ਨੂੰ ਪੱਧਰ ਕਰਨਾ </option>

                                                <option value="5" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Forest Fire / ਜੰਗਲ ਦੀ ਅੱਗ' ? 'selected="selected"' : ''); ?>> Forest Fire / ਜੰਗਲ ਦੀ ਅੱਗ </option>

                                                <option value="6" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Illegal Grazing on Forest Land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਨਜਾਇਜ਼ ਚਰਾਈ' ? 'selected="selected"' : ''); ?>> Illegal Grazing on Forest Land / ਜੰਗਲ ਦੀ ਜ਼ਮੀਨ ਤੇ ਨਜਾਇਜ਼ ਚਰਾਈ </option>

                                                <option value="7" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Accidental Tree Fall / ਦੁਰਘਟਨਾ/ਮੋਸਮੀ ਕਾਰਨਾਂ ਨਾਲ ਰੁੱਖਾਂ ਦਾ ਡਿੱਗਣਾ' ? 'selected="selected"' : ''); ?>> Accidental Tree Fall / ਦੁਰਘਟਨਾ/ਮੋਸਮੀ ਕਾਰਨਾਂ ਨਾਲ ਰੁੱਖਾਂ ਦਾ ਡਿੱਗਣਾ </option>

                                                <option value="8" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Wildlife Offence / ਜੰਗਲੀ ਜੀਵ ਜ਼ੁਰਮ' ? 'selected="selected"' : ''); ?>> Wildlife Offence / ਜੰਗਲੀ ਜੀਵ ਜ਼ੁਰਮ </option>

                                                <option value="9" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Wildlife Accidents / ਜੰਗਲੀ ਜੀਵ ਦੁਰਘਟਨਾਵਾਂ' ? 'selected="selected"' : ''); ?>> Wildlife Accidents / ਜੰਗਲੀ ਜੀਵ ਦੁਰਘਟਨਾਵਾਂ </option>

                                                <option value="10" <?php echo e($view_particular_complaint['nature_of_complaint']->nature_of_complaint_name == 'Others / ਹੋਰ' ? 'selected="selected"' : ''); ?>> Others / ਹੋਰ </option>
                                               
                                            </select>
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                        <label class="col-lg-3 col-form-label"> Status: </label>
                                        <div class="col-lg-5">
                                            <select class="form-control" name="status">
                                                <option> --select-- </option>
                                                <option value="close" <?php echo e($view_particular_complaint->status == 'close' ? 'selected="selected"' : ''); ?>> close </option>
                                                <option value="open" <?php echo e($view_particular_complaint->status == 'open' ? 'selected="selected"' : ''); ?>> open </option>
                                                <option value="inprogress" <?php echo e($view_particular_complaint->status == 'inprogress' ? 'selected="selected"' : ''); ?>> inprogress </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                        <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/complaints/edit_complaint.blade.php ENDPATH**/ ?>