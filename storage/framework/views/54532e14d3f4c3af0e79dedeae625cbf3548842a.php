
<?php $__env->startSection('content'); ?>

<div style="padding-top: 2%"></div>
<div class="page-content-wrapper ">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            
            <ul class="page-breadcrumb">
                <li>
                <a href="#">Home</a>
                <i class="fa fa-circle"></i>
                </li>
                <li>
                <span>Dashboard</span>
                </li>
            </ul>

        </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Admin Dashboard</h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 blue">
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup"><?php echo e($users); ?></span>
                            </div>
                            <div class="desc"> Total Users </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 red">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup"><?php echo e($complaints); ?></span> </div>
                            <div class="desc"> Total Complaints </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 green">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup"><?php echo e($open_complaints); ?></span>
                            </div>
                            <div class="desc"> Open Complaints </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 purple">
                        <div class="visual">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup"></span><?php echo e($closed_complaints); ?> </div>
                            <div class="desc"> Closed Complaints </div>
                        </div>
                    </a>
                </div>
            </div>
        <div class="clearfix"></div>
    <!-- END DASHBOARD STATS 1-->
        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div> <h4>Total No Of Complaints</h4></div>
                    <figure class="highcharts-figure">
                        <div id="tendays"></div>
                    </figure>

                </div>
                <!-- END PORTLET-->
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div><h4>Close Complaints</h4></div>
                    <figure class="highcharts-figure">
                        <div id="closedcomplaints"></div>

                    </figure>
                    <!-- END PORTLET-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('page-script'); ?>
<!-- graph script -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    Highcharts.chart('tendays', {
        credits: {
    enabled: false
},
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        max: 5,
        title: {
            text: 'Total Complaints'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: ''
    },
    series: [{
        name: '',
     data:[ 
        <?php foreach($tendays as $tenday){
            $result[]= "['".$tenday['date']."',".$tenday['ucount']."]";
        } echo implode(',',$result);?>
         ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
            
        }
    }]
});
</script>
<!-- close complaint grpah -->
   
<script>
    Highcharts.chart('closedcomplaints', {
        credits: {
    enabled: false
},
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        max:5,
        title: {
            text: 'Close Complaints'
        }
    },
    legend: {
        enabled: false
    },
    series: [{
       
       data:[ 
        <?php foreach($close_complaint as $complaintclose){
            $results[]= "['".$complaintclose['day']."',".$complaintclose['ccount']."]";
        } echo implode(',',$results);?>
         ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
            
        }
    }]
});
</script>
<script>

  var x = document.getElementsByClassName("highcharts-credits");
  x.style.display = "none";
   
</script>
    
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/admin_dashboard.blade.php ENDPATH**/ ?>