<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class GuardAction extends Model
{
    protected $primaryKey = 'guard_id';
    protected $table = 'pun_forest_guard_actions';
}