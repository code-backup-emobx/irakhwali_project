<?php echo $__env->make('layouts.top_bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

   
            <!-- BEGIN sidebar -->
            <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- END sidebar -->
            <div class="content">
            <?php echo $__env->make('layouts.notification', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                   
           <?php echo $__env->yieldContent('content'); ?>
            <!-- BEGIN FOOTER -->
            <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- END FOOTER --><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/layouts/master.blade.php ENDPATH**/ ?>