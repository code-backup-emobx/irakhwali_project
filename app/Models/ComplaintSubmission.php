<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ComplaintSubmission extends Model{

    protected $primaryKey = 'c_id';
    protected $table = 'user_complaints';

    public function complaint_media(){

    	return $this->hasMany('App\Models\ComplaintMedia', 'c_id');
    }
    
    public function complaint_description(){

    	return $this->hasMany('App\Models\ComplaintEditMultiple','c_id');
    }


    public function nature_of_complaint(){

    	return $this->belongsTo('App\Models\NatureOfComplaint', 'noc_id');
    }
}