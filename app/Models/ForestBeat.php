<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestBeat extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_beat';
   	public $timestamps = false;

   	public function forestblock(){

    	return $this->belongsTo('App\Models\ForestBlock');
    }
}