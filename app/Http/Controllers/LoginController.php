<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\Models\AllUsers;
use App\Models\ForestDivision;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    
   
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    public function login(Request $request){
       // $dfo = Hash::make('admin@1234'); 
         // return $dfo; die(); 
//        $obj = new Utilities();
//        $obj->pre($request->all());
        $password = $request->password;
        // super_admin
        if (\Auth::attempt(['name' => $request->name, 'password' => $request->password, 'user_type'=> 'super_admin'])) {
            // Authentication passed...
            return redirect()->intended('/');
        }
        // ACS
        elseif (\Auth::attempt(['password' => $request->password, 'user_type'=> 'acs'])) {
            return redirect()->intended('/');
        }
        //PCCF
        elseif (\Auth::attempt(['password' => $request->password, 'user_type'=> 'pccf'])) {
            return redirect()->intended('/');
        }
        // APCCF login
        elseif (\Auth::attempt(['name' => $request->division, 'password' => $request->password, 'user_type'=> 'APCCF-Admin'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->division, 'password' => $request->password, 'user_type'=> 'APCCF_Dev'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->division, 'password' => $request->password, 'user_type'=> 'APCCF_FCA'])) {
            return redirect()->intended('/');
        }
        //CCF/CF login
        elseif (\Auth::attempt(['name' => $request->circle, 'password' => $request->password, 'user_type'=> 'ccf_hill_zone'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->circle, 'password' => $request->password, 'user_type'=> 'ccf_plain_zone'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->circle, 'password' => $request->password, 'user_type'=> 'ccf_bist_circle'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->circle, 'password' => $request->password, 'user_type'=> 'ccf_ferozpur'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->circle, 'password' => $request->password, 'user_type'=> 'ccf_north_circle'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->circle, 'password' => $request->password, 'user_type'=> 'CCF_Shiwalik'])) {
            return redirect()->intended('/');
        }
        elseif (\Auth::attempt(['name' => $request->circle, 'password' => $request->password, 'user_type'=> 'ccf_south_circle'])) {
            return redirect()->intended('/');
        }
        // dfo login
        elseif (\Auth::attempt(['name' => $request->division_name, 'password' => $request->password, 'user_type'=> 'division'])) {
            return redirect()->intended('/');
        }
      
        else{
            return redirect()->back()->with('fail','!Enter the valid email and password');
        }
    }

    public function forest_division(){

    	$forest_division = ForestDivision::all();
    	return view('auth/login', compact('forest_division'));
    }

}

?>