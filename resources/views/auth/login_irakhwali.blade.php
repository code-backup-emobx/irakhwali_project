<!-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 -->

 @extends('layouts.login_tmp')
    <!-- begin::Body -->
@section('content')
    <!-- Begin logo -->
     <div class="logo">
            <a href="">
                <img src="{{asset('assets/img/irakhwali.png')}}" alt="" /> </a>
                <!-- <h3>iHariyali</h3> -->
        </div>

        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="" method="post" id="my_form">
                <h3 class="form-title">Login to your account</h3>
               
               <div class="form-actions">
                   <input type="button" name="pccf" id="pccf"  value="PCCF" class="btn pulse-btn green">
                   <input type="button" name="acs" id="acs"  value="ACS" class="btn pulse-btn green">
                   <input type="button" name="apccf" id="apccf"  value="APCCF" class="btn pulse-btn green">
                   <input type="button" name="dfo" id="dfo"  value="DFO" class="btn pulse-btn green">
                    <input type="button" name="cfnccf" id="cfnccf"  value="CCF/CF" class="btn pulse-btn green">
                   <input type="button" id="super" name="super" value="Site Admin" class="btn pulse-btn green">
                       
                    <!-- <a href="index.php" class="btn pulse-btn green pull-right"> Login </a> -->
                </div>

                 

                <div class="form-group" id="superadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                     <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" id="username" name="username" />  
                       
                </div>
                <div class="form-group" id="pccfadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                       
                </div>
                 <div class="form-group" id="apccfadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->


                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">DFO</label>
                    <div class="input-icon">
                        <select name="division" class="form-control">
                            <option>Select account</option>
                            <option>abc</option>
                            <option>abc</option>
                            <option>abc</option>

                        </select>
                    </div>
                
                       
                </div>
                <div class="form-group"  id="dfoadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">DFO</label>
                    <div class="input-icon">
                        <select name="division" class="form-control">
                            <option>Select Division</option>
                            <option>abc</option>
                            <option>abc</option>
                            <option>abc</option>
                        </select>
                    </div>
                </div>
                 
                <div class="form-group"  id="cfnccfadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">CCF/CF</label>
                    <div class="input-icon">
                        <select name="circle" class="form-control" >
                            <option>Select Name</option>
                            <option>abc</option>
                            <option>abc</option>
                            <option>abc</option>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                   <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> 
                        
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Remember me
                        <span></span>
                    </label>
                   <!--  <input type="submit" name="login" value="Login" class="btn pulse-btn green pull-right"> -->
                     <input type="submit" class="btn pulse-btn green pull-right"  name="login" value="Login">

                    <!-- <a href="index.php" class="btn pulse-btn green pull-right"> Login </a> -->
                </div>
                <!-- <div class="create-account">
                    <p> Don't have an account yet ?&nbsp;
                        <a href="register.html" id="register-btn"> Register </a>
                    </p>
                </div> -->
            </form>
            <!-- END LOGIN FORM -->
        </div>
@endsection
