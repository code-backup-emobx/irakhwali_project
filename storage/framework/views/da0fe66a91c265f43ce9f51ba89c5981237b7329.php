
<?php $__env->startSection('content'); ?>
        
 <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                         <div class="caption">
                                            <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i>
                                            <span class="caption-subject">Division Data</span>
                                        </div>
                                        
                                    </div>
                                  </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                            <th>Division List</th>
                                            <th>Add Detail</th>
                                        <tr>
                                          
                                            
                                            <?php $__currentLoopData = $division_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $listing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td>&nbsp;&nbsp;<?php echo e($listing->id); ?>.&nbsp;&nbsp;<?php echo e($listing->division_name); ?> </td>
                                                <td><a href="<?php echo e(url('/add_dfodetail/'.$listing->id)); ?>" class="btn btn-success" >Add detail</a></td>
                                                
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          
                                        </tbody>
                                    </table>
                                        </div>
                                    
                                    </div>
                                </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
       
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/divisiondata/division_data.blade.php ENDPATH**/ ?>