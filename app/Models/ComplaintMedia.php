<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ComplaintMedia extends Model
{
    protected $primaryKey = 'm_id';
    protected $table = 'complaint_media';
    public $timestamps = false;
}