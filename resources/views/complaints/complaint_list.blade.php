@extends('layouts.master')
@section('content')
        
 <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                         @if(Session::has('success'))

                                        <div class="alert alert-success">

                                            {{ Session::get('success') }}

                                            @php

                                            Session::forget('success');

                                            @endphp

                                        </div>

                                        @endif
                                        
                                        <div class="caption">
                                            <!-- <i class="icon-bubble font-dark"></i> -->
                                            <span class="caption-subject font-dark bold">Complaint list</span>
                                        </div>
                                        <div class="col-md-10">
                                        <div class="col-lg-3 action" style="margin-left:5px">
                                            <form class="form-horizontal" method="GET" action="{{ '/complaint_list' }}">
                                            <select name="status" id="status" class="form-control">
                                                
                                                <option value="all" @if($status == 'all') selected="all" @endif>All</option>
                                                <option value="open_after_7"  @if($status == 'open_after_7') selected="open_after_7" @endif>Open(7 days over)</option>
                                                <option value="open_before_7"  @if($status == 'open_before_7') selected="open_before_7" @endif>Open(7 days not complete)</option>
                                                <option value="inprogress" @if($status == 'inprogress') selected="inprogress" @endif>Inprogress</option>
                                                <option value="close" @if($status == 'close') selected="close" @endif>Resolved</option>
                                            </select>
                                        </div>
                                            <input type="submit" class="btn btn-success" value="Submit" style="float:left; margin-right:20% ">
                                            </form>

                                            <div class="col-lg-3 action">
                                                <input type="search" class="form-control" id="search" value="{{ $search }}"  placeholder="search by description" required>&nbsp;
                                            </div>
                                                <button type="button" class="btn btn-success" onclick="goToSearchUrl('search')";>Search</button>
                                                &nbsp;<a href="/complaint_list" class="btn btn-success">Clear</a>
                                    </div>
                                  </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Description </th>
                                                        <th> Nature Of Complaint</th>
                                                        <th> Date </th>
                                                        <th> Division </th>
                                                        <th> Range </th>
                                                        <th> Block </th>
                                                        <th> Beat </th>
                                                        <th> Status </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                            @if($data->total()==0)
                                                <tr><td colspan="12"><center><h5>" No record Found "</h5></center></td></tr>
                                            @endif
                                            
                                            <!--status wise data listing  -->
                                             
                                            @foreach($data as $listing)
                                            <tr
                                    
                                            @if($listing->status =='open' && $status=='open_after_7') style='background-color:#eb7369;font-weight: bolder;' 
                                            
                                            @elseif($listing->status =='close') style='background-color:#a0f1a0'

                                            @elseif($listing->status =='inprogress') style='background-color:#ffa466'  
                                           
                                            
                                            @elseif($listing->status=='open' && $listing->created_at <=   date('Y-m-d', strtotime("-8 days", strtotime(date('y-m-d')))))
                                            style='background-color:#eb7369;font-weight: bolder;'
                                            

                                            @endif style="margin-top:3%">
                                                <td> {{$listing->c_id}} </td>
                                                <td> {{$listing->c_description}} </td>
                                                <td> {{$listing->nature_of_complaint['nature_of_complaint_name'] }} </td>
                                                <td> {{$listing->created_at}} </td>
                                                <td>{{$listing->division}}</td>
                                                <td>{{$listing->range}}</td>
                                                <td>{{$listing->block}}</td>
                                                <td>{{$listing->beat}}</td>
                                                <td> {{$listing->status}} </td>
                                                <td><a href="{{ url('edit_complaint/'.$listing->c_id) }}" class="btn btn-success" > View  </a></td>
                                            </tr>
                                            @endforeach
                                          
                                        </tbody>
                                    </table>
                                        </div>
                                     {{ $data->appends(Request::except('page'))->links() }} 
                                    </div>
                                </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
       
@endsection

@push('page-script')
<script>
function goToSearchUrl(id){
    var search=$('#'+id).val();
    window.location.href="/complaint_list/"+search;
}
</script>


<!-- try code -->
<!-- <script>
let elmSelect = document.getElementById('status');

if (!!elmSelect) {
    elmSelect.addEventListener('change', e => {
        let choice = e.target.value;
        if (!choice) return;

        let url = new URL(window.location.href);
        url.searchParams.set('status', choice);
        // console.log(url);
        window.location.href = url; // reloads the page
    });
}
</script> -->
@endpush