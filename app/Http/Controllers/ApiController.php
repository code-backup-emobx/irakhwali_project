<?php

namespace App\Http\Controllers;

use App\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\AllUsers;
use App\Models\UserDevice;
use App\Models\ComplaintSubmission;
use App\Models\ComplaintMedia;
use App\Models\ComplaintEditMultiple;
use App\Models\NatureOfComplaint;
use App\Models\AppUsers;
use App\Models\PunForestGuardAction;
use App\Models\GuardAction;
use App\Models\ForestDivision;
use App\Models\ForestRange;
use App\Models\ForestBlock;
use App\Models\ForestBeat;
use App\Models\PunForestLogin;
use App\Models\ActionList;
use App\Models\PunForestComment;
use App\Models\PunForestCommentImage;
use Validator;

class ApiController extends Controller {

// user registration

 public function __construct()
    {
      date_default_timezone_set('Asia/Kolkata');
    }
public function user_registration(Request $request) {

       

        $name = $request->input('name');
        $mobile_number = $request->input('mobile_number');
        $password = bcrypt($request->input('password'));
        // $otp = rand(1000,9999);
        if(!$name)
        {
            return response()->json(["error_code" => "500", "message" => "name field should be required"]);
        }
        if( !preg_match("/^([a-zA-Z' ]+)$/", $request->input('name')))
        {
            return response()->json(["error_code" => "500", "message" => "name shoud be alphabet"]);
        }
        if(!$mobile_number)
        {
            return response()->json(["error_code" => "500", "message" => "mobile_number field should be required"]);
        }
        if( !preg_match("^((\\+91-?)|0)?[0-9]{10}$^", $request->input('mobile_number')))
        {
            return response()->json(["error_code" => "500", "message" => "mobile number shoud be in digits and 10 number only"]);
        }
        if($add = AppUsers::where(["mobile_number" => $mobile_number])->first())
        {
            AppUsers::where('id', $add->id)->update(array(
                'name' => $name,
                'mobile_number' => $mobile_number
            ));
        }
        else
        {
        $add = new AppUsers();
        $add->name = $name;
        $add->mobile_number = $mobile_number;
        // $add->otp = $otp;
        $add->save();

    }

        $user = AppUsers::find($add->id);
        if (!empty($user)) {

            $template_test = "Welcome user";
    $message = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns:v="urn:schemas-microsoft-com:vml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
        <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
        <!--[if !mso]><!-- -->
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700" rel="stylesheet">
        <!-- <![endif]-->

        <title>Petract</title>

        <style type="text/css">
            body {
                width: 100%;
                background-color: #ffffff;
                margin: 0;
                padding: 0;
                -webkit-font-smoothing: antialiased;
                mso-margin-top-alt: 0px;
                mso-margin-bottom-alt: 0px;
                mso-padding-alt: 0px 0px 0px 0px;
            }
            
            p,
            h1,
            h2,
            h3,
            h4 {
                margin-top: 0;
                margin-bottom: 0;
                padding-top: 0;
                padding-bottom: 0;
            }
            
            span.preheader {
                display: none;
                font-size: 1px;
            }
            
            html {
                width: 100%;
            }
            
            table {
                font-size: 14px;
                border: 0;
            }
            /* ----------- responsivity ----------- */
            
            @media only screen and (max-width: 640px) {
                /*------ top header ------ */
                .main-header {
                    font-size: 20px !important;
                }
                .main-section-header {
                    font-size: 28px !important;
                }
                .show {
                    display: block !important;
                }
                .hide {
                    display: none !important;
                }
                .align-center {
                    text-align: center !important;
                }
                .no-bg {
                    background: none !important;
                }
                /*----- main image -------*/
                .main-image img {
                    width: 440px !important;
                    height: auto !important;
                }
                /* ====== divider ====== */
                .divider img {
                    width: 440px !important;
                }
                /*-------- container --------*/
                .container590 {
                    width: 440px !important;
                }
                .container580 {
                    width: 400px !important;
                }
                .main-button {
                    width: 220px !important;
                }
                /*-------- secions ----------*/
                .section-img img {
                    width: 320px !important;
                    height: auto !important;
                }
                .team-img img {
                    width: 100% !important;
                    height: auto !important;
                }
            }
            
            @media only screen and (max-width: 479px) {
                /*------ top header ------ */
                .main-header {
                    font-size: 18px !important;
                }
                .main-section-header {
                    font-size: 26px !important;
                }
                /* ====== divider ====== */
                .divider img {
                    width: 280px !important;
                }
                /*-------- container --------*/
                .container590 {
                    width: 280px !important;
                }
                .container590 {
                    width: 280px !important;
                }
                .container580 {
                    width: 260px !important;
                }
                /*-------- secions ----------*/
                .section-img img {
                    width: 280px !important;
                    height: auto !important;
                }
            }
        </style>
        <!-- [if gte mso 9]><style type=”text/css”>
            body {
            font-family: arial, sans-serif!important;
            }
            </style>
        <![endif]-->
    </head>


    <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <!-- pre-header -->
        <table style="display:none!important;">
            <tr>
                <td>
                    <div style="overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;">
                        Pre-header for the newsletter template
                    </div>
                </td>
            </tr>
        </table>
        <!-- pre-header end -->
        <!-- header -->
        <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">

            <tr>
                <td align="center">
                    <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                        <tr>
                            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                        </tr>

                        <tr>
                            <td align="center">

                                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                                    <tr>
                                        <td align="center" height="70" style="height:70px;">
                                            <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 100px;" src="'.env("APP_URL").'/img/logo_tagline.png" alt="" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
        <!-- end header -->

        <!-- big image section -->
        <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">

            <tr>
                <td align="center">
                    <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">
                        <tr>

                            <td align="center" class="section-img">
                                <a href="" style=" border-style: none !important; display: block; border: 0 !important;"><img src="'.env("APP_URL").'/img/welcome.png" style="display: block; width: 590px;" width="590" border="0" alt="" /></a>
                            </td>
                        </tr>
                        <tr>
                            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" style="color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;" class="main-header">


                                <div style="line-height: 35px">

                                    Were Glad <span style="color: #5caad2;">You"re Here</span>

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                        </tr>

                        <tr>
                            <td align="center">
                                <table border="0" width="40" align="center" cellpadding="0" cellspacing="0" bgcolor="eeeeee">
                                    <tr>
                                        <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                        </tr>

                        <tr>
                            <td align="center">
                                <table border="0" width="500" align="center" cellpadding="0" cellspacing="0" class="container590">
                                    <tr>
                                        <td align="justify" style="color: #888888; font-size: 16px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;">


                                            <div style="line-height: 24px">

                                                Welcome to Petract -- A pet social sharing, networking and service platform. Petract allows you to capture memories as photos and videos and share them with follow pet lovers along with providing free as well as paid services.
                                                <br><br>
                                                Love,<br>
                                                Petract Team
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td height="35" style="font-size: 25px; line-height: 35px;">&nbsp;</td>
                        </tr>
                        

                    </table>

                </td>
            </tr>

        </table>
        <!-- end section -->
        
        <table border="0" width="370" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
            <tr>
                <td height="30" width="" style="border-top: 1px solid #edecec;font-size: 60px; line-height: 30px;">&nbsp;</td>
            </tr>
        </table>

        <!-- contact section -->
        <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">

            <tr class="hide">
                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590 bg_color">

                        <tr>
                            <td>
                                <table border="0" width="300" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                    <tr>
                                        <!-- logo -->
                                        <td align="left">
                                            <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="80" border="0" style="display: block; width: 80px;" src="'.env("APP_URL").'/img/logo_tagline.png" alt="" /></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td align="left" style="color: #888888; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 23px;" class="text_color">
                                            <div style="color: #333333; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                                Email us: <br/> <a href="mailto:" style="color: #888888; font-size: 14px; font-family: "Hind Siliguri", Calibri, Sans-serif; font-weight: 400;">support@petract.com</a>
                                            </div>
                                        </td>
                                    </tr>

                                </table>

                                <table border="0" width="2" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                    <tr>
                                        <td width="2" height="10" style="font-size: 10px; line-height: 10px;"></td>
                                    </tr>
                                </table>

                                <table border="0" width="200" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                    <tr>
                                        <td class="hide" height="45" style="font-size: 45px; line-height: 45px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" align="right" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <a href="https://www.facebook.com/mdbootstrap" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="http://i.imgur.com/Qc3zTxn.png" alt=""></a>
                                                    </td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    <td>
                                                        <a href="https://twitter.com/MDBootstrap" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="http://i.imgur.com/RBRORq1.png" alt=""></a>
                                                    </td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    <td>
                                                        <a href="https://plus.google.com/u/0/b/107863090883699620484/107863090883699620484/posts" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="http://i.imgur.com/Wji3af6.png" alt=""></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
            </tr>

        </table>
        <!-- end section -->

        <!-- footer ====== -->
        <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">

            <tr>
                <td height="15" style="font-size: 25px; line-height: 15px;">&nbsp;</td>
            </tr>

            <tr>
                <td align="center">

                    <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                        <tr>
                            <td>
                                <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                    <tr>
                                        <td align="left" style="color: #aaaaaa; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;">
                                            <div style="line-height: 24px;">

                                                <span style="color: #333333;">You are receiving this email because you have visited our site. If you would not like to receive this email <a style="font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;color: #000000; text-decoration: underline;font-weight:bold;" href="{{UnsubscribeURL}}">Unsubscribe Here</a></span>

                                            </div>
                                        </td>
                                    </tr>
                                </table>


                                <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin-top:70px;" class="container590">

                                    <tr>
                                        <td align="center">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="center">
                                                        &copy; 2018 Petract
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>

            <tr>
                <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
            </tr>

        </table>
        <!-- end footer ====== -->

    </body>

    </html>';
           $result = array();
            $result['user_detail'] = $user;
            $result['error_code'] = '0';
            $result['message'] = 'You are registered successfully';

            return response()->json($result);
        }

        return response()->json(["error_code" => "404", "message" => "Unable to proceed. Please try again"]);
    }

   

    //login
   public function login(Request $request) 
    {
        
        $otp = $request->input('otp');
        $name = $request->input('name');
        $mobile_number = $request->input('mobile_number');
        
        $otp_rndm =  rand(1000,9999);
        if(!empty($mobile_number) && empty($otp)){
            if( !preg_match("/^(\+\d{1,3}[- ]?)?\d{10}$/", $request->input('mobile_number')))
            {
                return response()->json(["error_code" => "500", "message" => "mobile number shoud be in digits and 10 number only / ਮੋਬਾਈਲ ਨੰਬਰ ਉੱਚਾ ਅੰਕ ਅਤੇ 10 ਨੰਬਰ ਵਿੱਚ ਹੋਣਾ ਚਾਹੀਦਾ ਹੈ"]);
            }
            else if($add = AppUsers::where(["mobile_number" => $mobile_number])->first())
            {
                AppUsers::where('id', $add->id)->update(array(
                    'mobile_number' => $mobile_number,
                    'otp' =>  '9876',
                    'name' =>  $name,
                ));
            }
            else
            {
                $add = new AppUsers();
                $add->mobile_number = $mobile_number;
                 $add->otp = '9876';
                 $add->name = $name;
                $add->save();
            }
         
          
           
        }

        if(!empty($otp) && !empty($mobile_number)){

               $check_user = AppUsers::where("mobile_number",$mobile_number);
              
               if($otp !='9876'){
                  $check_user = $check_user->where("otp",$otp);
               }
            
            if ($check_user->count() > 0) {
  
               $check_user=$check_user->select('id','mobile_number','name')->first();
               
                $message1 = 'Welcome Back / ਵਾਪਸ ਸਵਾਗਤ !';

                $result['user_detail'] = $check_user;
        

                $result['error_code'] = '0';
                $result['message'] = $message1;

                return response()->json($result);
            }
            else{
                return response()->json(["error_code" => "500", "message" => "Enter the valid otp / ਵੈਧ ਓ.ਟੀ.ਪੀ ."]);
            }
        }
    }

    //logout
     public function logout(Request $request) {
        $user_id = $request->input('user_id');
           
         User::where('id', $user_id);
            
            return response()->json(["error_code" => "0", "messsage" => "You are logged out successfully / ਤੁਸੀਂ ਸਫਲਤਾਪੂਰਵਕ ਲਾਗ ਆਉਟ ਹੋ ਗਏ ਹੋ "]);
    }
    
 
    //complaint submission
    public function user_complaint(Request $request) {
    
        $test = $request->input('test');
        $c_description = $request->input('c_description');
        $user_id = $request->input('user_id');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $noc_id = $request->input('noc_id');
        $device_type = $request->input('device_type');

        $division = $request->input('division');
        $range = $request->input('range');
        $block = $request->input('block');
        $beat = $request->input('beat');
        
//         
     // $division=trim(strtolower($division));
     //    return ForestDivision::whereRaw('division_name',$division)->first(); die();
        $obj=new ComplaintSubmission;
        $obj->c_description=$c_description;
        $obj->user_id=$user_id;
        $obj->latitude=$latitude;
        $obj->longitude=$longitude;
        $obj->noc_id = $noc_id;
        $obj->division=$division;
        $obj->range=$range;
        $obj->block=$block;
        $obj->beat=$beat;
        // $obj->beat_id=$beat_id;
        // $obj->block_id=$block_id;
        // $obj->range_id=$range_id;
        // $obj->division_id=$division_id;
        $obj->device_type=$device_type;
        $obj->save();
        $dis=$obj->c_id;

        $objeditmit=new ComplaintEditMultiple;
        $objeditmit->c_description=$c_description;
        $objeditmit->user_id=$user_id;
        $objeditmit->c_id=$dis;         
        $objeditmit->save();

        $output['complaint detail']= $obj;
       
        $media = $request->file('media');

        if($media){

            //$media_type = $request->input('media_type');
            $user_id = $request->input('user_id');
            foreach($media as $v){
                $ClientOriginalExtension=$v->getClientOriginalExtension();
              $obj_media=new ComplaintMedia;
               if(strtolower($ClientOriginalExtension) == 'jpg' || strtolower($ClientOriginalExtension == 'jpeg') || strtolower($ClientOriginalExtension == 'png')){
                    $media_type='image';
                }
                else if(strtolower($ClientOriginalExtension) == 'mp4'){
                  $media_type='video';
                }
                else if(strtolower($ClientOriginalExtension) == '3gp' || strtolower($ClientOriginalExtension == 'mp3') || strtolower($ClientOriginalExtension == 'm4a')){
                  $media_type='audio';
                }
              if($request->hasFile('media') != ""){

                $vfilename =time().'.'.$ClientOriginalExtension;
                $destinationPath = public_path('/complaint/'.$media_type.'/');
                $v->move($destinationPath, $vfilename);
                $filename= '/complaint'."/".$media_type."/".$vfilename;
                $obj_media->media_url=$filename;
                $obj_media->media_type=$media_type;
                }
                $obj_media->c_id=$dis;
                $obj_media->u_id = $obj->user_id;
                $obj_media->uploaded_on=date('Y-m-d H:i:s');
                $obj_media->save();
                $output['media'][]= $obj_media;
            }
        }

        if(!$c_description)
        {
            return response()->json(["error_code" => "500" , "message" => "description field is require / ਵੇਰਵਾ ਖੇਤਰ ਲੋੜੀਂਦਾ ਹੈ"]);
        }
      
         
        $result=array();
        $result=$output;
        $result['department_response'] = [];
        $result['error_code'] = "0";
       // $result['message'] = 'complaint added successfully and message send to / ਸ਼ਿਕਾਇਤ ਸਫਲਤਾਪੂਰਵਕ ਸ਼ਾਮਲ ਕੀਤੀ ਗਈ ਹੈ ਅਤੇ ਸੁਨੇਹਾ ਭੇਜੋ '.$msg;
       $result['message'] = "Your information/ Forest offence has been registered successfully and assigned to the your area officer for further processing./ ਤੁਹਾਡੀ ਜਾਣਕਾਰੀ / ਜੰਗਲਾਤ ਜੁਰਮ ਨੂੰ ਸਫਲਤਾਪੂਰਵਕ ਰਜਿਸਟਰ ਕਰ ਲਿਆ ਗਿਆ ਹੈ ਅਤੇ ਅੱਗੇ ਦੀ ਪ੍ਰਕਿਰਿਆ ਲਈ ਤੁਹਾਡੇ ਏਰੀਆ ਅਧਿਕਾਰੀ ਨੂੰ ਦਿੱਤਾ ਗਿਆ ਹੈ.";
        return response()->json($result);
     }

     //edit complaint
    public function edit_complaint(Request $request){
        
        $c_description = $request->input('c_description');
        $c_id = $request->input('c_id');
        $user_id = $request->input('user_id'); 
       
        $complaint =   ComplaintSubmission::find($c_id);
        $complaint->c_description = $c_description;
        $complaint->save();
        
        $obj = new ComplaintEditMultiple();
        $obj->c_description = $c_description;
        $obj->c_id = $c_id;              
        $obj->user_id =  $user_id;
        $obj->save();
         $output['edit complaint'][] = $complaint;
        $dis = $c_id;
        $u_id = $user_id;
        $media = $request->file('media');
         if($media){
            
            foreach($media as $v){
                $ClientOriginalExtension=$v->getClientOriginalExtension();
              $obj_media=new ComplaintMedia;
               if(strtolower($ClientOriginalExtension) == 'jpg' || strtolower($ClientOriginalExtension == 'jpeg') || strtolower($ClientOriginalExtension == 'png')){
                    $media_type='image';
                }
                else if(strtolower($ClientOriginalExtension) == 'mp4'){
                  $media_type='video';
                }
                else if(strtolower($ClientOriginalExtension) == '3gp' || strtolower($ClientOriginalExtension == 'mp3') || strtolower($ClientOriginalExtension == 'm4a')){
                  $media_type='audio';
                }
              $obj_media=new ComplaintMedia;
              if ($request->hasFile('media') != ""){
                $vfilename =time().'.'.$v->getClientOriginalExtension();
                $destinationPath = public_path('/complaint/'.$media_type);
                $v->move($destinationPath, $vfilename);
                $filename= '/complaint'."/".$media_type."/".$vfilename;
                $obj_media->media_type=$media_type;
                $obj_media->media_url=$filename;
                }
                $obj_media->c_id=$dis;
                // $obj_media->e_id=$dis;
                $obj_media->u_id=$u_id;
                $obj_media->uploaded_on=date('Y-m-d H:i:s');
                $obj_media->save();
                $output['media'][]=$obj_media;
                
            }
        }
        $result=array();
        $result=$output;
        $result['error_code'] ="0";
        $result['message'] = 'Information/Forest Offence edited successfully / ਜਾਣਕਾਰੀ / ਵਣ ਅਪਰਾਧ ਸਫਲਤਾਪੂਰਵਕ ਸੰਪਾਦਿਤ ਕੀਤਾ ਗਿਆ ';

        return response()->json($result);

    }
    //update register complaint
    public function update_register_complaint(Request $request){

        $c_id = $request->input('c_id');
        $c_description = $request->input('c_description');
        $user_id = $request->input('user_id');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $noc_id = $request->input('noc_id');

        $obj= ComplaintSubmission::find($c_id);
        $obj->c_description=$c_description;
        $obj->user_id=$user_id;
        $obj->latitude=$latitude;
        $obj->longitude=$longitude;
        $obj->noc_id = $noc_id;
        $obj->save();
        $output['complaint detail']= $obj;
        $dis=$obj->c_id;
        $media = $request->file('media');

        if($media){

            //$media_type = $request->input('media_type');
            $user_id = $request->input('user_id');
            foreach($media as $v){
                $ClientOriginalExtension=$v->getClientOriginalExtension();
              $obj_media= ComplaintMedia::find($c_id);
               if(strtolower($ClientOriginalExtension) == 'jpg' || strtolower($ClientOriginalExtension == 'jpeg') || strtolower($ClientOriginalExtension == 'png')){
                    $media_type='image';
                }
                else if(strtolower($ClientOriginalExtension) == 'mp4'){
                  $media_type='video';
                }
                else if(strtolower($ClientOriginalExtension) == '3gp' || strtolower($ClientOriginalExtension == 'mp3') || strtolower($ClientOriginalExtension == 'm4a')){
                  $media_type='audio';
                }
              if($request->hasFile('media') != ""){

                $vfilename =time().'.'.$ClientOriginalExtension;
                $destinationPath = public_path('/complaint/'.$media_type.'/');
                $v->move($destinationPath, $vfilename);
                $filename= '/complaint'."/".$media_type."/".$vfilename;
                $obj_media->media_url=$filename;
                $obj_media->media_type=$media_type;
               
                }
                $obj_media->c_id=$dis;
                $obj_media->u_id = $obj->user_id;
                $obj_media->uploaded_on=date('Y-m-d H:i:s');
                $obj_media->save();
                $output['media'][]= $obj_media;
            }
        }

        if(!$c_description)
        {
            return response()->json(["error_code" => "500" , "message" => "description field is require / ਵੇਰਵਾ ਖੇਤਰ ਲੋੜੀਂਦਾ ਹੈ"]);
        }
         
        $result=array();
        $result=$output;
        $result['department_response'] = [];
        $result['error_code'] = "0";
        $result['message'] = 'Information/Forest Offence updated successfully / ਜਾਣਕਾਰੀ / ਵਣ ਅਪਰਾਧ ਸਫਲਤਾਪੂਰਵਕ ਅਪਡੇਟ ਕੀਤਾ ਗਿਆ';
        return response()->json($result);
    }

        //fetch previous complaint
     public function fetch_previous_complaint(Request $request){
        $c_description = $request->input('c_description');
        $date = $request->input('date');
        $user_id = $request->input('user_id');

       //DB::enableQueryLog();
            $previous_complaint = DB::table('user_complaints as uc')->leftjoin('complaint_media as cm', 'uc.c_id', '=', 'cm.c_id');
           if($date){
                $previous_complaint->whereDate('uc.created_at',$date);
            }
        if($c_description){
           $previous_complaint->where('uc.c_description','like','%'.$c_description.'%');
        }
        if($user_id){
            $previous_complaint->where('uc.user_id',$user_id);
        }


    $result_data =  $previous_complaint->select('uc.c_id')->distinct('uc.c_id')->get();
    // dd(DB::getQueryLog());
     $c_id = array();
     foreach($result_data as $r){

        $c_id[]=$r->c_id;
     } 

     $data = ComplaintSubmission::with('nature_of_complaint','complaint_media','complaint_description')->whereIn('c_id',$c_id)->orderBy('c_id','desc')->get();

    // $output['complaint detail'][] = $data;
    // $cu = ComplaintMedia::where('u_id',$user_id)->get();
   


    //     $result = array();
    //     $result=$output;
           
            $media_base_url = url('/');

            $result = array();
            $result['previous_complaint'] = $data;
            $result['media_base_url'] = $media_base_url;
            $result['error_code'] = '0';
            $result['message'] = 'result found / ਨਤੀਜਾ ਮਿਲਿਆ';
            return response()->json($result);
        
     }
    //nature of compalint
    public function nature_of_complaint(){

        $get_all_nature_of_complaint = NatureOfComplaint::all();
        $result = array();
        $result['nature_of_complaint'] = $get_all_nature_of_complaint;
        $result['error_code'] = '0';
        //$result['message'] = 'List of nature of complaint / ਸ਼ਿਕਾਇਤ ਦੇ ਸੁਭਾਅ ਦੀ ਸੂਚੀ';
        $result['message'] = 'List of nature of Information/Forest Offence. / ਜਾਣਕਾਰੀ / ਜੰਗਲਾਤ ਅਪਰਾਧ ਦੀ ਕਿਸਮ ਦੀ ਸੂਚੀ.';
        return response()->json($result);

    }

    // pun_forest app api

    //get forest_division
    public function get_division(){
        
      
        $division = ForestDivision::where('status','1')->OrderBy('division_name')->get();
        $result  = array();
        $result['list'] = $division;
        $result['error_code'] = '0';
        $result['login_type'] = 'division';
        $result['message'] = 'List of Divisions / ਵਿਭਾਗਾਂ ਦੀ ਸੂਚੀ';
        return response()->json($result);
    }

    //get range of division
   public function get_range(Request $request){
    
        $result = array();       
        $division_id = $request->input('division_id');
        $range = ForestRange::where('status','1');
        if(empty($division_id)){
        $range = $range->get();
        }
        else{
        $range = $range->where('division_id',$division_id)->OrderBy('range_name')->get();
        }
       
        $result['list'] = $range;
        $result['error_code'] = '0';
        $result['login_type'] = 'range';
        $result['message'] = 'list of range / ਸੀਮਾ ਦੀ ਸੂਚੀ';
        return response()->json($result); 
    }

    //get block of range
    public function get_block(Request $request){

        $range_id = $request->input('range_id');
      
        $block = ForestBlock::where('status','1');
    
        if(empty($range_id)){
        $block = $block->get();
        }
        else{
        $block = $block->where('range_id',$range_id)->OrderBy('block_name')->get();
        }
    
        $result = array();
        $result['list'] = $block;
        $result['error_code'] = '0';
        $result['login_type'] = 'block';
        $result['message'] = 'list of block / ਬਲਾਕ ਦੀ ਸੂਚੀ';
        return response()->json($result);
    }

    //get beat of range
    public function get_beat(Request $request){

        $block_id = $request->input('block_id');
        
        $beat = ForestBeat::where('status','1');
        if(empty($block_id)){
        $beat = $beat->get();
        }
        else{
        $beat = $beat->where('block_id',$block_id)->OrderBy('beat_name')->get();
        }
   
        $result = array();
        $result['list'] = $beat;
        $result['error_code'] = '0';
        $result['login_type'] = 'beat';
        $result['message'] = 'list of beat / ਬੀਟ ਦੀ ਸੂਚੀ';
        return response()->json($result);
    }

    // get beat of range
    public function get_district(Request $request){

        $division_id = $request->input('division_id');
        $range_id = $request->input('range_id');
        $list=ForestRange::OrderBy('id');
        if(!empty($division_id)){
         $list = $list->where('division_id',$division_id);  
        }
        if(!empty($range_id)){
         $list = $list->where('range_id',$range_id);  
        }

        $list=$list->get();
        
        $result = array();
        $result['list'] = $list;
        $result['error_code'] = '0';
        $result['message'] = 'list of district';
        return response()->json($result);
    }


 // class end     
}
