  
  
  <?php $__env->startSection('content'); ?>

    <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <?php if(Session::has('success')): ?>

                                <div class="alert alert-success">

                                    <?php echo e(Session::get('success')); ?>


                                        <?php

                                        Session::forget('success');

                                        ?>

                                </div>

                                <?php endif; ?>
                            <div class="caption">
                                <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i>
                                <span class="caption-subject">Add Guard Detail</span>
                            </div>
                            <div class="col-lg-6"></div>
                            <div class="col-lg-3 action">
                                               
                            </div>
                            <a onclick="history.go(-1)" class="btn btn-success">Beat List</a>
                                       
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="<?php echo e(url('/add_guard_detail/'.$guard_detail->id)); ?>" class="form-horizontal" method="post">
                                  <?php echo e(csrf_field()); ?>

                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Beat Name: </label>
                                        <div class="col-lg-6">
                                            <?php if(!$guard_detail->g_name): ?>
                                                <input type="text" class="form-control" placeholder="Enter name" name="name" value="" required>
                                                <?php else: ?>
                                                <input type="text" class="form-control" placeholder="Enter address" name="name" value="<?php echo e($guard_detail->g_name); ?>" required>
                                            <?php endif; ?>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Contact Number: </label>
                                        <div class="col-lg-6">
                                           <?php if(!$guard_detail->g_contact): ?>
                                                <input type="text" class="form-control" placeholder="Enter Contact Number" name="contact" value="" required>
                                                <?php else: ?>
                                                <input type="text" class="form-control" placeholder="Enter contact" name="contact" value="<?php echo e($guard_detail->g_contact); ?>" required>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                             <?php if(!($guard_detail->g_name) && !($guard_detail->g_contact)): ?>
                                           <input type="submit" class="btn btn-success" value="Submit">
                                            <?php else: ?>
                                            <input type="submit" class="btn btn-success" value="Edit">
                                            <?php endif; ?>
                                             <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/divisiondata/add_guard_detail.blade.php ENDPATH**/ ?>