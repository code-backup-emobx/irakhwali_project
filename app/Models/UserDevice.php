<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;



class UserDevice extends Model

{

    /**

     * The table associated with the model.

     *

     * @var string

     */
 
    protected $table = 'user_devices';

    public $timestamps = false;

    protected $primaryKey = "id";

 
   
 

}