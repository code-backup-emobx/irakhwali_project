  
  @extends('layouts.master')
  @section('content')

    <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            @if(Session::has('success'))

                                <div class="alert alert-success">

                                    {{ Session::get('success') }}

                                        @php

                                        Session::forget('success');

                                        @endphp

                                </div>

                                @endif
                            <div class="caption">
                                <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i>
                                <span class="caption-subject">Add DFO Detail</span>
                            </div>
                            <div class="col-lg-6"></div>
                            <div class="col-lg-3 action">
                                               
                            </div>
                           <a onclick="history.go(-1)" class="btn btn-success">Division List</a>
                            </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{ url('/add_dfodetail/'.$division_detail->id) }}" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">DFO Name: </label>
                                        <div class="col-lg-6">
                                            @if(!$division_detail->dfo_name)
                                                <input type="text" class="form-control" placeholder="Enter name" name="name" value="" required>
                                                @else
                                                <input type="text" class="form-control" placeholder="Enter address" name="name" value="{{$division_detail->dfo_name}}" required>
                                            @endif
                                        </div>
                                       
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Contact Number: </label>
                                        <div class="col-lg-6">
                                           @if(!$division_detail->dfo_contact)
                                                <input type="text" class="form-control" placeholder="Enter Contact Number" name="contact" value="" required>
                                                @else
                                                <input type="text" class="form-control" placeholder="Enter contact" name="contact" value="{{$division_detail->dfo_contact}}" required>
                                            @endif
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                             @if(!($division_detail->dfo_name) && !($division_detail->dfo_contact))
                                           <input type="submit" class="btn btn-success" value="Submit">
                                            @else
                                            <input type="submit" class="btn btn-success" value="Edit">
                                            @endif
                                            <a href="{{ url('/range/'.$division_detail->id) }}" class="btn btn-success">Go to Range</a>
                                             
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

