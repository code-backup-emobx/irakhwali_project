<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
Use App\Models\AllUsers;
Use App\Models\ComplaintSubmission;
Use App\Models\ComplaintMedia;
Use App\Models\AppUsers;
use App\User;
use App\Models\ForestDivision;
use App\Models\ForestRange;
use App\Models\ForestBlock;
use App\Models\ForestBeat;
use Validator;


class DivisionController extends Controller
{
	public function __construct()
    {
      $this->middleware('auth');
    } 

	public function division_data(){
		$division_data = ForestDivision::where('status','=','1')->get();
		return view('divisiondata/division_data',compact('division_data'));
	} 
	//view the officer area filter select 
	public function areaofficer_data(Request $request , $id = null)
	{

		// DB::enableQueryLog();
		// $division_id = $request->input('division_id');
		$division_id = $id;
		
		// $division_detail = ForestDivision::where('id',$division_id)->first();
							

		$area_detail = ForestDivision::where('id',$division_id)
							->with('forestranges','forestblocks','forestbeats')
							// ->with('forestblocks')
							// ->with('forestbeats')
							->first();

		// dd($area_detail);
							
		$division_list = ForestDivision::all();
		
		return view('divisiondata/area_officer_data',
			compact(
				'division_list',
				'division_id',
				'area_detail'
			)
		);
	}
	// add dfo detail 
	public function edit_dfodetail($id){
		$division_detail = ForestDivision::where('id',$id)->where('status','=','1')->first();
		return view('divisiondata/add_dfodetail',compact('division_detail'));
	}

	//add dfo name and contact
	public function add_dfo_detail(Request $request , $id){

		$name = $request->input('name');
		$contact = $request->input('contact');
		if( !preg_match("/^([a-zA-Z' ]+)$/", $request->input('name')))
        {
           return redirect('/add_dfodetail/'.$id)->with('success','name shoud be alphabet');
        }
        if( !preg_match("/^[0-9]{10}$/", $request->input('contact')))
        {
            return redirect('/add_dfodetail/'.$id)->with('success','contact number shoud be in digits and 10 number only');
        }
		$add_dfodetail = ForestDivision::where('id',$id)->update(['dfo_name' => $name , 'dfo_contact'=>$contact]);
		return redirect('/add_dfodetail/'.$id)->with('success', 'Successfully added the detail');
	}

	// particular division range list 

	public function range_detail($id)
	{
		$division_data = ForestDivision::where('status','=','1')->where('id','=',$id)->first();
		$range_list = ForestRange::where('division_id','=',$id)->where('status','=','1')->get();
		return view('divisiondata/range',compact('range_list','division_data'));
	} 

	//range officer detail
	public function rodetail($id){
		$back_to_range_list = ForestDivision::where('status','=','1')->where('id','=',$id)->first();
		// return $back_to_range_list;die();
		$range_officer_detail = ForestRange::where('id','=',$id)->first();
		return view('divisiondata/add_rodetail',compact('range_officer_detail','back_to_range_list'));
	}
	public function add_ro_detail(Request $request , $id){
		
		$name = $request->input('name');
		$contact = $request->input('contact');
		if( !preg_match("/^([a-zA-Z' ]+)$/", $request->input('name')))
        {
           return redirect('/add_rodetail/'.$id)->with('success','name shoud be alphabet');
        }
        if( !preg_match("/^[0-9]{10}$/", $request->input('contact')))
        {
            return redirect('/add_rodetail/'.$id)->with('success','contact number shoud be in digits and 10 number only');
        }
		$add_rodetail = ForestRange::where('id',$id)->update(['ro_name' => $name , 'ro_contact'=>$contact]);
		return redirect('/add_rodetail/'.$id)->with('success', 'Successfully added the detail');
	}

	//block list 
	public function block_list($id){

		$block_list = ForestBlock::where('range_id','=',$id)->where('status','=','1')->get();
		return view('divisiondata/block',compact('block_list'));
	}

	public function bodetail($id){

		$block_detail = ForestBlock::where('id','=',$id)->where('status','=','1')->first();
		return view('divisiondata/add_bodetail',compact('block_detail'));
	}
	
	public function add_bo_detail(Request $request , $id){
		
		$name = $request->input('name');
		$contact = $request->input('contact');
		if( !preg_match("/^([a-zA-Z' ]+)$/", $request->input('name')))
        {
           return redirect('/add_bodetail/'.$id)->with('success','name shoud be alphabet');
        }
        if( !preg_match("/^[0-9]{10}$/", $request->input('contact')))
        {
            return redirect('/add_bodetail/'.$id)->with('success','contact number shoud be in digits and 10 number only');
        }
		$add_bodetail = ForestBlock::where('id',$id)->update(['bo_name' => $name , 'bo_contact'=>$contact]);
		return redirect('/add_bodetail/'.$id)->with('success', 'Successfully added the detail');
	}

	//beat list 
	public function beat_list($id){

		$beat_list = ForestBeat::where('block_id','=',$id)->where('status','=','1')->get();
		return view('divisiondata/beat',compact('beat_list'));
	}
	public function guard_detail($id){

		$guard_detail = ForestBeat::where('id',$id)->where('status','=','1')->first();
		return view('divisiondata/add_guard_detail',compact('guard_detail'));
	}
  
   public function  add_guard_detail(Request $request , $id){
		
		$name = $request->input('name');
		$contact = $request->input('contact');
		if( !preg_match("/^([a-zA-Z' ]+)$/", $request->input('name')))
        {
           return redirect('/add_guard_detail/'.$id)->with('success','name shoud be alphabet');
        }
        if( !preg_match("/^[0-9]{10}$/", $request->input('contact')))
        {
            return redirect('/add_guard_detail/'.$id)->with('success','contact number shoud be in digits and 10 number only');
        }
		$add_guard_detail = ForestBeat::where('id',$id)->update(['g_name' => $name , 'g_contact'=>$contact]);
		return redirect('/add_guard_detail/'.$id)->with('success', 'Successfully added the detail');
	}

}
   
  
     

    

