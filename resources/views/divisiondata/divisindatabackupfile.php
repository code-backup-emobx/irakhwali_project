  @extends('layouts.master')
  @section('content')

    <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i>
                                <span class="caption-subject">Division Data</span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                           
                            <form action=" " class="form-horizontal" method="GET">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <h4><b>Divisions Name</b></h4>
                                    <!-- 1 -->
                                    <a id="super" name="super" value="Site Admin" >1.&nbsp;Nawanshahr&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row">
                                        <div class="col-lg-8">
                                            <div class="form-group" id="superadmin" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                 <input type="submit" class="btn btn-success" value="Go to range">
                                            </div>
                                        </div>
                                    </div> 

                                          <!-- 2 -->
                                           <a id="supers" name="super" value="Site Admin" >2.&nbsp;Mohali&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="superadmins" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 3 -->
                                    <a id="ropar" name="super" value="Site Admin" >3.&nbsp;Ropar&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="ropardivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 4 -->
                                     <a id="Hoshiarpur" name="super" value="Site Admin" >4.&nbsp;Hoshiarpur&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Hoshiarpurdivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 5 -->
                                     <a id="Pathankot" name="super" value="Site Admin" >5.&nbsp;Pathankot&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Pathankotdivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 6 -->
                                     <a id="Patiala" name="super" value="Site Admin" >6.&nbsp;Patiala&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Patialadivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 7 -->
                                     <a id="Sangrur" name="super" value="Site Admin" >7.&nbsp;Sangrur&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Sangrurdivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 8 -->
                                     <a id="Mansa" name="super" value="Site Admin" >8.&nbsp;Mansa&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Mansadivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                     <!-- 9 -->
                                     <a id="Ludhiana" name="super" value="Site Admin" >9.&nbsp;Ludhiana&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Ludhianadivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                     <!-- 10 -->
                                     <a id="Bhatinda" name="super" value="Site Admin" >10.&nbsp;Bhatinda&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Bhatindadivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 11 -->
                                     <a id="Sri_Mukatsarsahib" name="super" value="Site Admin" >11.&nbsp;Sri Mukatsarsahib&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Sri_Mukatsarsahibdivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>

                                     <!-- 12 -->
                                     <a id="Ferozepur" name="super" value="Site Admin" >12.&nbsp;Ferozepur&nbsp;<!-- <i class="glyphicon glyphicon-arrow-right"></i> --></a>
                                    <div class="form-group row" >
                                        <div class="col-lg-8">
                                            <div class="form-group" id="Ferozepurdivision" style="display:none;padding-top: 1%">
                                                <div class="col-lg-4">   
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="DFO Name" id="username" name="name" />  
                                                </div>
                                                <div class="col-lg-4">    
                                                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="phone-number" id="password" name="password" /> 
                                                </div> 
                                                <input type="submit" class="btn btn-success" value="Submit">
                                                <input type="submit" class="btn btn-success" value="Go To Range">
                                            </div>
                                        </div>
                                    </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

@push('page-script')
    <script>
            
        $("#super").on("click", function(){
            $('#superadmin').show();
            $('#username').val('');
        });

        $("#supers").on("click", function(){
            $('#superadmins').show();
            $('#username').val('');
        });
      
        
         $("#ropar").on("click", function(){
            $('#ropardivision').show();
            $('#username').val('');
        });
       
        $("#Hoshiarpur").on("click", function(){
            $('#Hoshiarpurdivision').show();
            $('#username').val('');
        });
       
        $("#Pathankot").on("click", function(){
            $('#Pathankotdivision').show();
            $('#username').val('');
        });

        $("#Patiala").on("click", function(){
            $('#Patialadivision').show();
            $('#username').val('');
        });
         
        $("#Sangrur").on("click", function(){
        $('#Sangrurdivision').show();
        $('#username').val('');
        });

       
        $("#Mansa").on("click", function(){
        $('#Mansadivision').show();
        $('#username').val('');
        });
       
        $("#Ludhiana").on("click", function(){
        $('#Ludhianadivision').show();
        $('#username').val('');
        });
      
        $("#Bhatinda").on("click", function(){
        $('#Bhatindadivision').show();
        $('#username').val('');
        });
        
        $("#Sri_Mukatsarsahib").on("click", function(){
        $('#Sri_Mukatsarsahibdivision').show();
        $('#username').val('');
        });
       
        $("#Ferozepur").on("click", function(){
        $('#Ferozepurdivision').show();
        $('#username').val('');
        });

    </script>
@endpush