<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class NatureOfComplaint extends Model
{
    protected $primaryKey = 'noc_id';
    protected $table = 'nature_of_complaint';
   
}