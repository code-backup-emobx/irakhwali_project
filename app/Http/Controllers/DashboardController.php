<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
Use App\Models\AllUsers;
Use App\Models\ComplaintSubmission;
use App\User;

use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

   
    public function showCountdataDashboard()
    { 
     
      
      $data = array();
      $data['users'] = AllUsers::count();
      $data['complaints'] = ComplaintSubmission::count();
      $data['open_complaints'] = ComplaintSubmission::where('status','open')->count();
      $data['closed_complaints'] = ComplaintSubmission::where('status','close')->count();
      $data['tendays']= $this->ComplaintCount(); 
       $data['close_complaint']= $this->CloseComplaint();
      return view('admin_dashboard',$data);
    }

    public function ComplaintCount(){
        $lastTenDates[]= $currentdate = date('Y-m-d');
        $lastTenDates[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-7 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-8 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate)));
       
        foreach($lastTenDates as $date_value ){
            $d=array();
             $d['date']=date('l',strtotime($date_value));
             $d['ucount']=ComplaintSubmission::whereDate('created_at',$date_value)->count();
             $dd[]=$d;
        }
      return $dd;
    }
   public function CloseComplaint(){

      $closecomplaint[] = $currentdate = date('Y-m-d');
      $closecomplaint[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-7 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-8 day", strtotime($currentdate)));
      $closecomplaint[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate))); 

      foreach($closecomplaint as $value)
      {
        $complaint_value = array();
        $complaint_value['day']= date('l',strtotime($value));
         $complaint_value['ccount']=ComplaintSubmission::whereDate('updated_at',$value)->where('status','close')->count();
       // $complaint_value['ccount']=ComplaintSubmission::whereDate('updated_at','2020-11-09')->count();
        $data[]=$complaint_value;
        //print_r($data);die();
      }
      return $data;
    }
  
}
     