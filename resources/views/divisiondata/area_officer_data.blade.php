@extends('layouts.master')
@section('content')
        
<div style="padding-top: 2%"></div>
    <div class="page-content-wrapper">
        <div class="page-content">
            
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">

                            @if(Session::has('success'))

                                <div class="alert alert-success">

                                    {{ Session::get('success') }}

                                    @php

                                    Session::forget('success');

                                    @endphp

                                </div>

                            @endif
                            
                            <div class="caption">
                                <!-- <i class="icon-bubble font-dark"></i> -->
                                <span class="caption-subject font-dark bold">Officer Area list</span>
                            </div>

                            <div class="col-md-10">
                                <div class="col-lg-3 action" style="margin-left:5px">
                                    <form class="form-horizontal">
                                        <select name="division_id" id="division_id"  class="form-control">
                                            <option value="">Select Division</option>
                                            @foreach($division_list as $listing)
                                              <option value="{{$listing->id}}" @if($division_id == $listing->id) selected @endif >{{$listing->division_name}}</option>
                                            @endforeach
                                        </select>
                                    </div> 
                                        <input type="button" class="btn btn-success" value="Submit" style="float:left; margin-right:20% "  onclick="return mysubmit();">
                                    </form>

                                </div>
                            </div>
                         

                            @if(Request::segment(2)) 

                            <div class="portlet-body">
                                @if(!empty($area_detail))
                                    <div class="table-scrollable">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr> 
                                                    <td colspan="2">Division Name</td> 
                                                    <td colspan="2">{{$area_detail->division_name}}</td> 
                                               </tr> 
                                               <tr> 
                                                    <td>Dfo Name</td> 
                                                    <td>{{$area_detail->dfo_name}}</td> 
                                                    <td>Phone No</td> 
                                                    <td>{{$area_detail->dfo_contact}}</td>
                                               </tr> 
                                           </tbody>
                                        </table>
                                    </div>
                                @endif
                                <!-- 2 -->
                                <?php //echo "<pre>"; print_r($area_detail); die(); ?>
                                <?php //echo "<pre>"; print_r($area_detail[0]['forestblocks']); die(); ?>

                                @foreach($area_detail['forestranges'] as $forestrange)
                                    <div class="row" style="padding-left:2.5%;padding-right:1%;">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr> 
                                                        <td colspan="2">Range Name</td> 
                                                        <td colspan="2">{{$forestrange->range_name}}</td>
                                                    </tr> 
                                                    <tr> 
                                                        <td>Ranger Officer</td> 
                                                        <td>{{$forestrange->ro_name}}</td> 
                                                        <td>Phone No</td> 
                                                        <td>{{$forestrange->ro_contact}}</td>
                                                    </tr> 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- 2 -->

                                    @foreach($area_detail['forestblocks'] as $forestblock)


                                        <?php //echo "<pre>"; print_r($area_detail); die(); ?>
                                       
                                        @if($forestblock->range_id ==  $forestrange->id)
                                            <div class="row" style="padding-left:3.5%;padding-right:1%;">
                                                <div class="table-scrollable">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr> 
                                                                <td colspan="2">Block Name</td> 
                                                                <td colspan="2">{{$forestblock->block_name}}</td>
                                                            </tr> 
                                                            <tr> 
                                                                <td>Block Officer</td> 
                                                                <td>{{$forestblock->bo_name}}</td> 
                                                                <td>Phone No</td> 
                                                                <td>{{$forestblock->bo_contact}}</td>
                                                            </tr> 
                                                        </tbody>
                                                    </table>
                                                </div> 
                                           </div>
                                       
                                  
                                            <!-- /2 -->
                                            <?php //echo "<pre>"; print_r($area_detail[0]['forestbeats']); die(); ?>
                                            <!-- 3 -->
                                            @foreach($area_detail['forestbeats'] as $forestbeat)
                                               
                                                
                                                @if($forestbeat->block_id ==  $forestblock->id)
                                                    
                                                    <div class="row" style="padding-left:4.5%;padding-right:1%;">
                                                        <div class="table-scrollable">
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                    <tr> 
                                                                        <td colspan="2">Beat Name</td> 
                                                                        <td colspan="2">{{$forestbeat->beat_name}}</td>
                                                                    </tr> 
                                                                    <tr> 
                                                                        <td>Guard Name</td> 
                                                                        <td>{{$forestbeat->g_name}}</td> 
                                                                        <td>Phone No</td> 
                                                                        <td>{{$forestbeat->g_contact}}</td>
                                                                    </tr> 
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif

                                    @endforeach
                                       <!-- /3 -->
                                @endforeach
                            </div>

                            @endif
                                

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
    <!-- END CONTENT -->
    </div>
<script>
var crt_url = "<?php echo url('/'); ?>";
function mysubmit(){
  //you can return false; here to prevent the form being submitted
  //useful if you want to validate the form
  var id = $( "#division_id" ).val();
  
  uri =  crt_url + '/area_officer_data/'+ document.getElementById('division_id').value;
  window.location.href= uri;
}
</script>
       
@endsection

