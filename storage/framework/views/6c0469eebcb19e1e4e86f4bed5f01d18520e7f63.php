
<?php $__env->startSection('content'); ?>
        
 <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <!-- <i class="icon-bubble font-dark"></i> -->
                                            <span class="caption-subject font-dark bold">Users list</span>
                                        </div>
                                        <div class="col-lg-6"></div>
                                         <div class="col-lg-3 action">
                                                <input type="search" class="form-control" id="search" value="<?php echo e($search); ?>" placeholder="search by name" required>&nbsp;
                                            </div>
                                                <button type="button" class="btn btn-success" onclick="goToSearchUrl('search')";>Search</button>
                                                &nbsp;<a href="/user_list" class="btn btn-success">Clear</a>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Name </th>
                                                        <th> Mobile Number </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                            <?php if($userlist->total()==0): ?>
                                                <tr><td colspan="12"><center><h5>" No record Found "</h5></center></td></tr>
                                            <?php endif; ?>
                                           
                                            <?php $__currentLoopData = $userlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $listing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td> <?php echo e($listing->id); ?> </td>
                                                <td> <?php echo e($listing->name); ?> </td>
                                                <td> <?php echo e($listing->mobile_number); ?> </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                    <?php echo e($userlist->links()); ?>

                                        </div>
                                    </div>
                                </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
       
<?php $__env->stopSection(); ?>

 <?php $__env->startPush('page-script'); ?>
<script>
function goToSearchUrl(id){
    var search=$('#'+id).val();
    window.location.href="/user_list/"+search;
}
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/user/user_list.blade.php ENDPATH**/ ?>