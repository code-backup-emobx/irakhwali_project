<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestDivision extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_division';
    public $timestamps = false;

   	public function forestranges(){

    	return $this->hasMany('App\Models\ForestRange','division_id');
    }

    public function forestblocks(){

		return $this->hasManyThrough(
	        'App\Models\ForestBlock',
	        'App\Models\ForestRange',
	        'division_id', // Foreign key on ForestRange table...
	        'range_id' // Foreign key on ForestBlock table...
	    );
	}

	public function forestbeats(){

	    return $this->hasManyThrough(
	        'App\Models\ForestBlock',
	        'App\Models\ForestRange',
	        'division_id', // Foreign key on ForestRange table...
	        'range_id' // Foreign key on ForestBlock table...
	    )->join('forest_beat','forest_block.id','=','forest_beat.block_id')->select('forest_beat.*');
	}
}