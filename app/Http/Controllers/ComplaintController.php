<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
Use App\Models\AllUsers;
Use App\Models\ComplaintSubmission;
Use App\Models\ComplaintMedia;
Use App\Models\NatureOfComplaint;
Use App\Models\ComplaintEditMultiple;
use App\User;
use Validator;

class ComplaintController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function complaintList(Request $request , $search="" ){
      
       $status = $request->input('status'); 
      $complaint_detail_status =  ComplaintSubmission::with('nature_of_complaint')->where('status',$status)->orderBy('c_id', 'DESC')->where('c_description','like','%'.$search.'%')->paginate(10);
      
      $all_complaint = ComplaintSubmission::with('nature_of_complaint')->orderBy('c_id', 'DESC')->where('c_description','like','%'.$search.'%')->paginate(10);

      // open status days count complete 7 days 
       $currentdate = date('Y-m-d');
       $dayscount = date('Y-m-d', strtotime("-8 days", strtotime($currentdate)));
       $open_over_seven_days = ComplaintSubmission::with('nature_of_complaint')->whereDate('created_at','<=',$dayscount)->where('status','open')->orderBy('c_id', 'DESC')->where('c_description','like','%'.$search.'%')->paginate(10);
       //return $open_over_seven_days; die();

       // open status count 7 days not completed
       $curr_date = date('Y-m-d');
       $dayscounts = date('Y-m-d', strtotime("-6 days", strtotime($curr_date)));
       $open_under_seven_days = ComplaintSubmission::with('nature_of_complaint')->where('status','open')->whereDate('created_at','>=',$dayscounts)->orderBy('c_id', 'DESC')->where('c_description','like','%'.$search.'%')->paginate(10);
       //return $open_under_seven_days;

      $complaint_list = ComplaintSubmission::with('nature_of_complaint')->where('c_description','like','%'.$search.'%')->paginate(10);
      //return ($complaint_list);die();
     
      if($status =='close' || $status =='inprogress'){
        $data=$complaint_detail_status;
      }
      else if($status =='open_after_7'){
        $data=$open_over_seven_days;
      }
      else if($status =='open_before_7'){
        $data=$open_under_seven_days;
      }
      else {
      $data=$all_complaint;
      }
      //return $data; 

      return view('complaints/complaint_list',compact('data','status','search'));
    }

    public function editComplaint($id){

      $complaint_media = ComplaintMedia::where('c_id',$id)->get();
       // return $complaint_media; die();
      $view_multiple_complaint = ComplaintSubmission::with('complaint_description')->where('c_id',$id)->first();
       // return $view_multiple_complaint; die();

      $view_particular_complaint = ComplaintSubmission::with('complaint_media','nature_of_complaint')->where('c_id',$id)->first();
      
      return view('complaints/edit_complaint',compact('view_particular_complaint','view_multiple_complaint','complaint_media'));
    }

    public function updateComplaint(Request $request , $id){
      
      $status = $request->input('status');
      // $nature_of_complaint = $request->nature_of_complaint;

      $update_complaint = ComplaintSubmission::where('c_id',$id)->update(['status' => $status]);
      return redirect('/complaint_list')->with('success', 'Successfully updated');
    }

   
}
   
  
     

    

