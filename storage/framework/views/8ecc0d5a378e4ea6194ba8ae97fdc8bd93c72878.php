    <!-- begin::Body -->
<?php $__env->startSection('content'); ?>
    <!--  -->
     <div class="logo">
            <a href="">
                <img src="<?php echo e(asset('assets/img/irakhwali.png')); ?>" alt="" /> </a>
                <!-- <h3>iHariyali</h3> -->
        </div>

        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo e(url('/login')); ?>" method="post" id="my_form">
                <?php echo e(csrf_field()); ?>

                <h3 class="form-title">Login to your account</h3>
               
               <div class="form-actions">
                   <input type="button" name="pccf" id="pccf"  value="PCCF" class="btn pulse-btn green">
                   <input type="button" name="acs" id="acs"  value="ACS" class="btn pulse-btn green">
                   <input type="button" name="apccf" id="apccf"  value="APCCF" class="btn pulse-btn green">
                   <input type="button" name="dfo" id="dfo"  value="DFO" class="btn pulse-btn green">
                    <input type="button" name="cfnccf" id="cfnccf"  value="CCF/CF" class="btn pulse-btn green">
                   <input type="button" id="super" name="super" value="Site Admin" class="btn pulse-btn green">
                       
                    <!-- <a href="index.php" class="btn pulse-btn green pull-right"> Login </a> -->
                </div>
                 

                <div class="form-group" id="superadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                     <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" id="username" name="name" />  
                       
                </div>
                <div class="form-group" id="pccfadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                       
                </div>
                 <div class="form-group" id="apccfadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->


                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">APCCF</label>
                    <div class="input-icon">
                        <select name="division" class="form-control">
                            <option>Select account</option>
                            <option value="APCCF-Admin">APCCF-Admin</option>
                            <option value="APCCF-Dev">APCCF-Dev</option>
                            <option value="APCCF-FCA">APCCF-FCA</option>
                        </select>
                    </div>
                
                       
                </div>
                <div class="form-group"  id="dfoadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">DFO</label>
                    <div class="input-icon">
                        <select name="division_name" class="form-control">
                            <option>Select Division</option>
                            <?php $__currentLoopData = $forest_division; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($division->division_name); ?>"><?php echo e($division->division_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
                 
                <div class="form-group"  id="cfnccfadmin" style="display:none">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">CCF/CF</label>
                    <div class="input-icon">
                        <select name="circle" class="form-control" >
                            <option>Select Name</option>
                            <option value="CCF_Hills_Zone">CCF_Hills_Zone</option>
                            <option value="CCF_Plain_Zone">CCF_Plain_Zone</option>
                            <option value="CCF_Bist_Circle">CCF_Bist_Circle</option>
                            <option value="CCF_Ferozpur">CCF_Ferozpur</option>
                            <option value="CCF_North_Circle">CCF_North_Circle</option>
                            <option value="CCF_Shiwalik">CCF_Shiwalik</option>
                            <option value="CCF_South_Circle">CCF_South_Circle</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                   <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> 
                        
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Remember me
                        <span></span>
                    </label>
                   <!--  <input type="submit" name="login" value="Login" class="btn pulse-btn green pull-right"> -->
                   <button type="submit" name="login_submit" class="btn pulse-btn green pull-right">Login</button>
                     <!-- <input type="submit" class="btn pulse-btn green pull-right"  name="login" value="Login"> -->

                    <!-- <a href="index.php" class="btn pulse-btn green pull-right"> Login </a> -->
                </div>
                <!-- <div class="create-account">
                    <p> Don't have an account yet ?&nbsp;
                        <a href="register.html" id="register-btn"> Register </a>
                    </p>
                </div> -->
            </form>
            <!-- END LOGIN FORM -->
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.login_tmp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\emobx\htdocs\irakhwali\resources\views/auth/login.blade.php ENDPATH**/ ?>